const checkData = new Promise(function(resolve,reject){
    console.log('Request data...')
    setTimeout(() => {
        console.log('Preparing data...');
        const data = {
            server: "aws",
            port: 2000,
            status: "pending",
        };
        if (data) {
            let message = "SUCCESS";
            resolve(message);
        } else {
            let message = "FAILED";
            reject (message);
        }
    }, 2000);
   
})

checkData.then(function(data) {
    setTimeout(() => {
        data.success = "success";
        console.log ("Data received:",data);
    }, 2000)
}).catch(function(data){
    console.log('error');
    console.log(data);
})
